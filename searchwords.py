#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras, hecho por Martin Jiménez Huete
'''



import sortwords
import sys



def search_word(word, words_list):
    found = False
    for i in words_list:
        if sortwords.equal(word, i):
            palabra = words_list.index(i)
            found = True
            return palabra

    if not found:
        raise Exception




def main():
    words_list = sys.argv[1:]
    word = sys.argv[1]

    if len(sys.argv) < 3:
        sys.exit("At least two arguments are needed")

    try:
        ordered_list = sortwords.sort(words_list)
        index = search_word(word, ordered_list)
        sortwords.show(ordered_list)
        print(index)
    except Exception:
        sys.exit("Word not found")


if __name__ == '__main__':
    main()

